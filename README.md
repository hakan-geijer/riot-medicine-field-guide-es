# Medicina para motines: Guia de campo

_This is a Spanish translation of the [*Riot Medicine: Field Guide*][riot-medicine-fg]_

[riot-medicine-fg]: https://gitlab.com/hakan-geijer/riot-medicine-field-guide-en/

## License

This project and its contents are in the public domain (CC0).
