% vim: spelllang=es_es
% original section: burns
\section{Quemaduras}

Las quemaduras son lesiones en la piel y otros tejidos causadas por el calor, el frío, los productos químicos y la radiación.

\subsection{Signos y síntomas}

\begin{table}[htb]
\caption{Clasificación de quemaduras}
\label{tbl:burn_classification}
\centering
\footnotesize

\begin{tabularx}{\textwidth}{|l|X|}
\hline
\textbf{Clasificación} & \textbf{Características} \\
\hline
Grave    & Espesor parcial \(>25\%\) SCT, 10--50 años \\
         & Espesor parcial \(>20\%\) SCT, \(<10\) o \(>50\) años \\
         & Espesor parcial total \(10\%\) SCT \\
         & Quemaduras en manos, cara, pies o ingle; o cruzando una articulación principal \\
         & Quemaduras circunferenciales en una extremidad \\
         & Lesión por inhalación  \\
         & Quemaduras eléctricas \\
         & Quemaduras complicadas por fracturas u otros traumatismos \\
\hline
Moderada & Espesor parcial 15--25\% SCT, 10--50 años\\
         & Espesor parcial 10--20\% SCT, \(<10\) o \(>50\) años \\
         & Espesor total \(\leq10\%\) \\
         & Sospecha de lesión por inhalación \\
         & Ausencia de características de quemadura grave \\
\hline
Leve     & Espesor parcial \(<15\%\) SCT, 10--50 años \\
         & Espesor parcial \(<10\%\) SCT, \(<10\) o \(>50\) años \\
         & Espesor total \(<2\%\) \\
         & Ausencia de características de quemadura grave \\
\hline
\end{tabularx}

SCT: superficie corporal total
\end{table}

Las quemaduras varían en gravedad según su tamaño, profundidad y ubicación.
Se pueden ver ejemplos de quemaduras a diferentes profundidades en fig.~\ref{fig:burn_characteristics}.
Las pautas de clasificación están en tbl.~\ref{tbl:burn_classification}.


\begin{itemize}
\tightlist
\item Profundidad de quemadura:
  \begin{itemize}
  \tightlist
  \item Superficial:
    \begin{itemize}
    \tightlist
    \item La piel está enrojecida, sensible, dolorosa y se torna blanca al presionarla
    \item \textbf{No} ampollas
    \end{itemize}
  \item Espesor parcial superficial:
    \begin{itemize}
    \tightlist
    \item La piel está enrojecida, sensible, extremadamente dolorosa y se torna blanca al presionarla
    \item Ampollas
    \item La dermis expuesta está roja y húmeda
    \end{itemize}
  \item Espesor parcial profundo:
    \begin{itemize}
    \tightlist
    \item La dermis es blanca o amarilla y no se torna blanca al presionarla
    \item Puede no ser dolorosa, solo tiene sensación de presión o malestar
    \end{itemize}
  \item Espesor total:
    \begin{itemize}
    \tightlist
    \item La piel está rígida, blanca o marrón, textura correoso y podría estar carbonizada
    \item Indoloro
    \end{itemize}
  \end{itemize}
\item Lesión por inhalación:
  \begin{itemize}
  \tightlist
  \item Quemaduras en la cara
  \item Vello facial chamuscado
  \item Vía respiratoria constreñida, tos, sibilancias o estridor
  \end{itemize}
\item Intoxicación por monóxido de carbono:
  \begin{itemize}
  \tightlist
  \item Dolor de cabeza, náusea, vómitos, letargia o debilidad
  \item Dolor de pecho (angina de pecho) o dificultad para respirar
  \item Ataxia, síncope, convulsiones o coma
  \end{itemize}
\end{itemize}

\begin{figure}[htb]
\centering

\begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includesvg[width=\textwidth, height=4cm, keepaspectratio]{superficial-burn-hand}
    \caption{Superficial}
\end{subfigure}
\begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includesvg[width=\textwidth, height=4cm, keepaspectratio]{superficial-partial-thickness-burn-hand}
    \caption{Espesor parcial superficial}
\end{subfigure}

\begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includesvg[width=\textwidth, height=4cm, keepaspectratio]{deep-partial-thickness-burn-hand}
    \caption{Espesor parcial profundo}
\end{subfigure}
\begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includesvg[width=\textwidth, height=4cm, keepaspectratio]{full-thickness-burn-hand}
    \caption{Espesor total}
\end{subfigure}

\caption{Características de las quemaduras}
\label{fig:burn_characteristics}
\end{figure}

\subsection{Tratamiento}

\begin{itemize}
\tightlist
\item For chemical burns:
  \begin{itemize}
  \tightlist
  \item Cepille o seque los productos químicos
  \item Enjuague con abundante agua
  \item Retire los lentes de contacto del paciente si están contaminados
  \end{itemize}
\item Para quemaduras térmicas:
  \begin{itemize}
  \tightlist
  \item Pare la quemadura (Deténgase, tírese, ruede)
  \item Enfríe rápidamente las quemaduras con agua
  \end{itemize}
\item Quite las joyas y la ropa:
  \begin{itemize}
  \tightlist
  \item \textbf{No} retire el material derretido en la piel
  \end{itemize}
\item Limpie e irrigue las heridas:
  \begin{itemize}
  \tightlist
  \item \textbf{No} reviente las ampollas
  \end{itemize}
\item Aplique antisépticos y vende las heridas:
  \begin{itemize}
  \tightlist
  \item Quemaduras \(<3\%\) SCT: apósitos húmedos
  \item Quemaduras \(>3\%\) SCT: apósitos secos
  \end{itemize}
\item Revise la presencia de lesiones por inhalación
\item Trate shock si esta presenta
\item Considere atención médica avanzada si:
\begin{itemize}
  \tightlist
  \item Quemaduras graves: centro de quemados
  \item Quemaduras moderadas: hospital
  \item Quemaduras menores: enviar a casa o a un médico genera
  \end{itemize}
\end{itemize}
